from django.contrib.auth.models import User
from django.db import models


# Create your models here.
class EmployeeProfile(models.Model):
    EMPLOYEE_TYPE = (
        (1, 'Admin'),
        (2, 'Sales Person'),
    )
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    phone = models.CharField(max_length=11, null=True, blank=True)
    latitude = models.FloatField(default=0.0)
    longitude = models.FloatField(default=0.0)
    employee_type = models.IntegerField(choices=EMPLOYEE_TYPE)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)


class Customer(models.Model):
    CUSTOMER_TYPE = (
        (1, 'Distributor'),
        (2, 'Retailer'),
    )
    full_name = models.CharField(max_length=100)
    shop_name = models.CharField(max_length=100)
    phone = models.CharField(max_length=11)
    address = models.CharField(max_length=100)
    email = models.EmailField(max_length=100)
    assigned_by = models.ForeignKey(User, on_delete=models.CASCADE)
    customer_type = models.IntegerField(choices=CUSTOMER_TYPE)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.full_name

    class Meta:
        db_table = "bh_customers"
