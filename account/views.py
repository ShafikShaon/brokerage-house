from .serializers import *
from .models import *
from rest_framework.viewsets import ModelViewSet
from django.contrib.auth.models import User


# Create your views here.
class AccountListViewSet(ModelViewSet):
    queryset = EmployeeProfile.objects.all()
    serializer_class = AccountListSerializer


class AccountViewSet(ModelViewSet):
    queryset = User.objects.all()
    serializer_class = AccountSerializer