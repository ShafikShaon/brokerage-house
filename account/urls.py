from rest_framework.routers import DefaultRouter
from .views import *

router = DefaultRouter()
router.register('all', AccountListViewSet)
router.register('login', AccountListViewSet)
urlpatterns = router.urls