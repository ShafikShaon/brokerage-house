from rest_framework.routers import DefaultRouter
from .views import *
from django.urls import path

router = DefaultRouter()
router.register('lists', ProductListViewSet)
router.register('category', ProductCategoryViewSet)
router.register('stocks', ProductStockViewSet)

urlpatterns = router.urls

urlpatterns  += [
    path(r'^create/', ProducCreatetView.as_view(), name="product-create" )
]
