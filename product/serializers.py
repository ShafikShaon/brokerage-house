from rest_framework import serializers
from .models import *


class ProductSerializer(serializers.ModelSerializer):
    product_category = serializers.StringRelatedField(many=False)

    class Meta:
        model = Product
        fields = ('id', 'product_name', 'product_price', 'created_at', 'product_category')


class ProductCategorySerializer(serializers.ModelSerializer):
    products = ProductSerializer(read_only=True)

    class Meta:
        model = ProductCategory
        fields = '__all__'


class StocksSerializer(serializers.ModelSerializer):
    product = serializers.StringRelatedField(many=False)

    class Meta:
        model = ProductStock
        fields=('id', 'unit', 'created_at', 'product')


class ProductAddSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields= ('product_name', 'product_price')


