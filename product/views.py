from .serializers import *
from .models import *
from rest_framework.viewsets import ModelViewSet
from rest_framework.views import APIView
from rest_framework.response import Response


# Create your views here.
class ProductListViewSet(ModelViewSet):
    queryset = Product.objects.all()
    serializer_class = ProductSerializer


class ProductCategoryViewSet(ModelViewSet):
    queryset = ProductCategory.objects.all()
    serializer_class = ProductCategorySerializer


class ProductStockViewSet(ModelViewSet):
    queryset = ProductStock.objects.all()
    serializer_class = StocksSerializer


# class ProductAddViewSet(APIView):
#     queryset = Product.objects.all()
#     serializer_class = ProductAddSerializer
class ProducCreatetView(APIView):
    """
    View to list all users in the system.

    * Requires token authentication.
    * Only admin users are able to access this view.
    """

    def post(self, request, validated_data):

        """
        Return a list of all users.
        """
        product = Product(
            product_name=validated_data.get('product_name'),
            product_price=456345,
            product_category_id=1
        )
        product.save()
        return Response({})