(function () {
    'use strict'
    angular.module('brokerage_house', ['ngRoute'])
        .controller('productController', ['$scope', '$http', productController]);
}());

function productController($scope, $http) {
    $scope.insertData=function(){
        console.log("caling");
            $http.post("/product/create/", {
                'product_name':$scope.product_name,
                'product_price':$scope.product_price,
                'product_category_id':1,
            }).then(function(response){
                    console.log("Data Inserted Successfully");
                },function(error){
                    alert("Sorry! Data Couldn't be inserted!");
                    console.error(error);

                });
            }
    $scope.data = [];
    $http.get('/product/lists/').then(function (response) {
        $scope.product_lists = response.data;
    });

    $http.get('/product/category/').then(function (response) {
        $scope.category_lists = response.data;
    });

    $http.get('/product/stocks/').then(function (response) {
        $scope.product_stocks = response.data;
    });
}
