(function () {
    'use strict'
    angular.module('brokerage_house', ['ngRoute'])
        .controller('accountController', ['$scope', '$http', accountController]);
}());

function accountController($scope, $http) {
    $scope.data = [];
    $http.get('/account/all/').then(function (response) {
        $scope.account_lists = response.data;
    });
}