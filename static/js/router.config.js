(function () {
    'use strict';

    angular.module('brokerage_house')
        .config(['$routeProvider', config])
        .run(['$http', run]);

    function config($routeProvider) {
        $routeProvider
            .when('/', {
                templateUrl:'//base.html',
            })
            .when('/account/all/', {
                templateUrl:'/static/views/account/account_all.html',
                controller: accountController,
            })
            .when('/account/login/', {
                templateUrl:'static/views/account/auth/login.html',
                controller: loginController,
            })
            .when('/product/list/', {
                templateUrl:'/static/views/product/all_products.html',
                controller: productController,
            })
            .when('/product/category/', {
                templateUrl:'/static/views/product/category.html',
                controller: productController,
            })
            .when('/product/stocks/', {
                templateUrl:'/static/views/product/stocks.html',
                controller: productController,
            })
            .when('/product/add/', {
                templateUrl:'/static/views/product/add_product.html',
                controller: productController,
            })
            .otherwise('/');
    }

    function run($http){
        $http.defaults.xsrfHeaderName = 'X-CSRFToken';
        $http.defaults.xsrfCookieName = 'csrftoken';
    }
})();